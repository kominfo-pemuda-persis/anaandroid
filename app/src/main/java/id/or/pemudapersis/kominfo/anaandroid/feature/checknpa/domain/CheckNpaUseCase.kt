package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain

import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaRequest
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.SendEmailRequest
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface CheckNpaUseCase {
    fun postCheckNpa(npa: CheckNpaRequest): Single<BaseResponse<CheckNpaResponse>>
    fun postSendEmail(data: SendEmailRequest): Single<BaseResponse<String>>
}

class CheckNpaUseCaseImpl @Inject constructor(private val repository: CheckNpaRepository): CheckNpaUseCase {
    override fun postCheckNpa(npa: CheckNpaRequest): Single<BaseResponse<CheckNpaResponse>> {
        return repository.postCheckNpa(npa)
    }

    override fun postSendEmail(data: SendEmailRequest): Single<BaseResponse<String>> {
        return repository.sendEmail(data)
    }
}