package id.or.pemudapersis.kominfo.anaandroid.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 *  Created by hanhan.firmansyah on 26/08/22
 */
@Parcelize
data class DetailStatus(
  @field:SerializedName("status") val status: String? = null,
  @field:SerializedName("jumlah") val jumlah: Int? = null
): Parcelable
