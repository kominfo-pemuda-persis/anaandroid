package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendEmailRequest(
    @field:SerializedName("npa") val npa: String? = null,
    @field:SerializedName("email") val email: String? = null,
): Parcelable
