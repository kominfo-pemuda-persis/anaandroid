package id.or.pemudapersis.kominfo.anaandroid.feature.wizard.callback

interface WizardCallback {
    fun onDataPass(): Boolean
}