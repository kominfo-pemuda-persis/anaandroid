package id.or.pemudapersis.kominfo.anaandroid.feature.login.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.or.pemudapersis.kominfo.anaandroid.feature.login.domain.LoginRepository
import id.or.pemudapersis.kominfo.anaandroid.feature.login.domain.LoginRepositoryImpl
import id.or.pemudapersis.kominfo.anaandroid.feature.main.data.MainRepository
import id.or.pemudapersis.kominfo.anaandroid.feature.main.data.MainRepositoryImpl
import javax.inject.Singleton

/**
 * Created by Julsapargi Nursam on 20/08/22
 * Mobile Engineer - Android
 */

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindsLoginRepository(
        loginRepositoryImpl: LoginRepositoryImpl
    ): LoginRepository

    @Binds
    @Singleton
    abstract fun bindsMainRepository(mainRepositoryImpl: MainRepositoryImpl): MainRepository
}