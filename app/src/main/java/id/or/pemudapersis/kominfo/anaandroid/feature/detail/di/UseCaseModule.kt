package id.or.pemudapersis.kominfo.anaandroid.feature.detail.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain.DetailAnggotaUseCase
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain.DetailAnggotaUseCaseImpl

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @Binds
    @ViewModelScoped
    abstract fun bindsDetailAnggotaUseCase(
        detailAnggotaUseCaseImpl: DetailAnggotaUseCaseImpl
    ): DetailAnggotaUseCase
}
