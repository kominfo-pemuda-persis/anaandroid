package id.or.pemudapersis.kominfo.anaandroid.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.or.pemudapersis.kominfo.anaandroid.base.util.Constants
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AnaStorageModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): SharedPreferences = context.getSharedPreferences(Constants.SHARED_NAME, Context.MODE_PRIVATE)
}