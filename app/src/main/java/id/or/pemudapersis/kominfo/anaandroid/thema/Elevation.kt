package id.or.pemudapersis.kominfo.anaandroid.thema

import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import javax.annotation.concurrent.Immutable

/**
 * Created by Julsapargi Nursam on 10/09/22
 * Mobile Engineer - Android
 */

@Immutable
data class AnaElevation(
    val default: Dp
)

val LocalAnaElevation = staticCompositionLocalOf {
    AnaElevation(
        default = Dp.Unspecified
    )
}

val anaElevation = AnaElevation(
    default = 8.dp
)