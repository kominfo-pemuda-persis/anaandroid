package id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain

import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.base.network.AnaService
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailAnggotaResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface DetailAnggotaRepository {
    fun fetchDetailAnggota(): Single<BaseResponse<DetailAnggotaResponse>>
}

class DetailAnggotaRepositoryImpl @Inject constructor(private val service: AnaService) :
    DetailAnggotaRepository {
    override fun fetchDetailAnggota(): Single<BaseResponse<DetailAnggotaResponse>> {
        return service.fetchDetailAnggota()
    }
}

