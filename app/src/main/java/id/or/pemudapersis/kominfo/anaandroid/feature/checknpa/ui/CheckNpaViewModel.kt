package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.ui

import dagger.hilt.android.lifecycle.HiltViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.BaseViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaRequest
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.SendEmailRequest
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain.CheckNpaUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class CheckNpaViewModel @Inject constructor(
    private val useCase: CheckNpaUseCase,
) : BaseViewModel<CheckNpaViewModel.ResponseState>() {

    fun postCheckNpa(npa: String) {
        val objNpa = CheckNpaRequest(npa)
        addDisposable.add(
            useCase.postCheckNpa(objNpa)
                .doOnSubscribe { _state.postValue(ResponseState.ShowLoading) }
                .doAfterTerminate { _state.postValue(ResponseState.HideLoading) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    // result response to display data
                    _state.value = ResponseState.ResponseData(result)
                }, {
                    // result response
                    _state.value = ResponseState.ResponseFailed(it, "result")
                })
        )
    }

    fun postSendEmail(npa: String, email: String) {
        val obj = SendEmailRequest(npa = npa, email = email)
        addDisposable.add(
            useCase.postSendEmail(obj)
                .doOnSubscribe { _state.postValue(ResponseState.ShowLoading) }
                .doAfterTerminate { _state.postValue(ResponseState.HideLoading) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    // result response to display data
                    _state.value = ResponseState.ResponseSendEmail(result)
                }, {
                    // result response
                    _state.value = ResponseState.ResponseFailed(it, "authorize")
                })
        )
    }

    sealed class ResponseState {
        object ShowLoading : ResponseState()
        object HideLoading : ResponseState()
        object ResponseSuccess : ResponseState()

        data class ResponseFailed(val error: Throwable, val isResult: String) : ResponseState()
        data class ResponseData(val data: BaseResponse<CheckNpaResponse>) : ResponseState()
        data class ResponseSendEmail(val data: BaseResponse<String>) : ResponseState()
    }
}