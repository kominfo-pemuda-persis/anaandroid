package id.or.pemudapersis.kominfo.anaandroid.feature.login.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.R
import id.or.pemudapersis.kominfo.anaandroid.base.BaseActivity
import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseErrorResponse
import id.or.pemudapersis.kominfo.anaandroid.databinding.ActivityLoginBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.ui.CheckNpaActivity
import id.or.pemudapersis.kominfo.anaandroid.feature.main.ui.MainActivity
import retrofit2.HttpException

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private val viewModel by viewModels<LoginViewModel>()

    override val bindingInflater: (LayoutInflater) -> ActivityLoginBinding
        get() = ActivityLoginBinding::inflate

    override fun setupView(binding: ActivityLoginBinding) {
        setupClick()
        setupObserver()
        setupFormListener()
    }

    override fun setData() {
        //bo implementation required

    }

    override fun setObserver(): FragmentActivity = this

    private fun setupClick() {
        with(binding) {
            btLogin.setOnClickListener {
                val npa = etNpa.text.toString().trim()
                val password = etPassword.text.toString()
                if (npa.isEmpty()) {
                    tilUsername.error = getString(R.string.text_npa_empty)
                }
                if (password.isEmpty()) {
                    tilPassword.error = getString(R.string.text_password_empty)
                }
                if (npa.isNotEmpty() && password.isNotEmpty()) {
                    viewModel.postLogin(npa, password)
                }
            }
            btRegister.setOnClickListener {
                val intent = Intent(this@LoginActivity, CheckNpaActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun setupFormListener() {
        with(binding) {
            tilUsername.editText?.doOnTextChanged { text, _, _, _ ->
                if (text?.isEmpty() == true) {
                    tilUsername.error = getString(R.string.text_npa_empty)
                } else {
                    tilUsername.error = null
                    tilUsername.isErrorEnabled = false
                }
            }

            tilPassword.editText?.doOnTextChanged { text, start, count, after ->
                if (text?.isEmpty() == true) {
                    tilPassword.error = getString(R.string.text_password_empty)
                } else {
                    tilPassword.error = null
                    tilPassword.isErrorEnabled = false
                }
            }
        }
    }

    private fun setupObserver() {
        viewModel.state.observe(this) { state ->
            when (state) {
                is LoginViewModel.LoginState.SHowLoading -> {
                    showLoading(true)
                }
                is LoginViewModel.LoginState.HideLoading -> {
                    showLoading(false)
                }
                is LoginViewModel.LoginState.LoginSuccess -> {
                    showLoading(false)
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                is LoginViewModel.LoginState.LoginFailed -> {
                    showLoading(false)
                    onLoginFailed(state.error)
                }
            }
        }
    }

    private fun onLoginFailed(throwable: Throwable) {
        val message = when (throwable) {
            is HttpException -> {
                Gson().fromJson(
                    throwable.response()?.errorBody()?.string(),
                    BaseErrorResponse::class.java
                ).errorDescription
            }
            else -> throwable.localizedMessage
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            if (isLoading) {
                pbLogin.visibility = View.VISIBLE
                enableView(!isLoading)
            } else {
                pbLogin.visibility = View.GONE
                enableView(!isLoading)
            }
        }
    }

    private fun enableView(isEnabled: Boolean) {
        with(binding) {
            btLogin.isEnabled = isEnabled
            btRegister.isEnabled = isEnabled
            etNpa.isEnabled = isEnabled
            etPassword.isEnabled = isEnabled
        }
    }
}