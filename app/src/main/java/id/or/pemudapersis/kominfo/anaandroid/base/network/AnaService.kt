package id.or.pemudapersis.kominfo.anaandroid.base.network

import id.or.pemudapersis.kominfo.anaandroid.base.model.*
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.*
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailAnggotaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.login.data.LoginResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*

interface AnaService {
    @FormUrlEncoded
    @POST("/oauth/login")
    fun login(@Field("username") npa: String,
              @Field("password") password: String,
              @Field("grant_type") grantType: String): Single<LoginResponse>

    @POST("/v1/api/anggota/check")
    fun checkNpa(@Body npa: CheckNpaRequest): Single<BaseResponse<CheckNpaResponse>>

    @POST("/v1/api/anggota/sendEmail")
    fun sendEmail(@Body npa: SendEmailRequest): Single<BaseResponse<String>>

    @GET("api/anggota/profile")
    fun fetchAnggota(): Single<BaseResponse<Anggota>>

    @GET("api/summaries/monografi")
    fun fetchSummariesMonografi(): Single<BaseResponse<MonografiItem>>

    @GET("api/summaries/performa")
    fun fetchSummariesPerforma(): Single<BaseResponse<PerformaItem>>

    @GET("api/summaries/status-keanggotaan")
    fun fetchStatusKeanggotaan(): Single<BaseResponse<StatusKeanggotaan>>

    @GET("api/anggota/profile/foto/detail")
    fun fetchFotoDetail(): Single<BaseResponse<FotoDetail>>

    @GET("api/anggota/profile")
    fun fetchDetailAnggota(): Single<BaseResponse<DetailAnggotaResponse>>
}