package id.or.pemudapersis.kominfo.anaandroid.feature.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.or.pemudapersis.kominfo.anaandroid.databinding.ItemDataKeluargaBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailKeluarga

class DetailKeluargaAdapter(private val detailsKeluarga: List<DetailKeluarga>) :
    RecyclerView.Adapter<DetailKeluargaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemDataKeluargaBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val detailKeluarga = detailsKeluarga[position]
        holder.bind(detailKeluarga)
    }

    override fun getItemCount() = detailsKeluarga.size

    inner class ViewHolder(private var binding: ItemDataKeluargaBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(detailKeluarga: DetailKeluarga) {
            binding.apply {
                tvNamaKeluargaData.text = detailKeluarga.namaKeluarga
                tvAlamatKeluargaData.text = detailKeluarga.alamat
                tvHubunganData.text = detailKeluarga.hubungan
            }
        }
    }
}
