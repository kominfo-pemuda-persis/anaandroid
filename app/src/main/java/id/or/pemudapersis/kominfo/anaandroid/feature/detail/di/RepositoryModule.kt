package id.or.pemudapersis.kominfo.anaandroid.feature.detail.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain.DetailAnggotaRepository
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain.DetailAnggotaRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindsDetailAnggotaRepository(
        detailAnggotaRepositoryImpl: DetailAnggotaRepositoryImpl
    ): DetailAnggotaRepository
}
