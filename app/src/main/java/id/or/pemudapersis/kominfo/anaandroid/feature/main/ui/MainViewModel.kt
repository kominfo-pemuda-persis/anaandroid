package id.or.pemudapersis.kominfo.anaandroid.feature.main.ui

import android.content.SharedPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.BaseViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.util.Constants
import id.or.pemudapersis.kominfo.anaandroid.base.model.Anggota
import id.or.pemudapersis.kominfo.anaandroid.base.model.FotoDetail
import id.or.pemudapersis.kominfo.anaandroid.feature.main.domain.MainUseCase
import id.or.pemudapersis.kominfo.anaandroid.feature.main.model.SummariesData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
  private val useCase: MainUseCase,
  private val sharedPreferences: SharedPreferences
) : BaseViewModel<MainViewModel.MainViewState>() {

  fun doLogout() {
    with(sharedPreferences.edit()) {
      remove(Constants.ACCESS_TOKEN)
      apply()
    }

  }

  fun fetchDataAnggota() {
    addDisposable.add(
      useCase.fetchAnggota()
        .doOnSubscribe { _state.postValue(MainViewState.ShowLoading) }
        .doOnSuccess { _state.postValue(MainViewState.HideLoading) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ response ->
          response.data?.let {
            _state.postValue(MainViewState.HideLoading)
            _state.postValue(MainViewState.GetDataAnggota(it))
          }
        }, {
          _state.postValue(MainViewState.HideLoading)
          _state.postValue(MainViewState.ErrorState(it))
        })
    )
  }

  fun fetchSummaryData() {
    addDisposable.add(
      useCase.fetchSummaries()
        .doOnSuccess { _state.postValue(MainViewState.HideLoading) }
        .subscribeOn(Schedulers.io())
        .subscribe({
          _state.apply {
            postValue(MainViewState.HideLoading)
            postValue(MainViewState.GetSummaries(it))
          }
        }, {
          _state.postValue(MainViewState.HideLoading)
          _state.postValue(MainViewState.ErrorState(it))
        })
    )
  }

  fun fetchFotoDetail() {
    addDisposable.add(
      useCase.fetchFotoDetail()
        .subscribeOn(Schedulers.io())
        .subscribe({ response ->
          response.data?.let {
            _state.apply {
              postValue(MainViewState.HideLoading)
              postValue(MainViewState.GetFotoDetail(it))
            }
          }
        }, {
          //No implementation required
        })
    )
  }

  //TODO: handle error code
  private fun handleErrorState(error: Throwable) {

  }

  sealed class MainViewState {
    object ShowLoading : MainViewState()
    object HideLoading : MainViewState()
    object EmptyResult : MainViewState()

    data class GetDataAnggota(val anggota: Anggota): MainViewState()
    data class ErrorState(val throwable: Throwable): MainViewState()
    data class GetSummaries(val summariesData: SummariesData): MainViewState()
    data class GetFotoDetail(val fotoDetail: FotoDetail): MainViewState()
  }
}