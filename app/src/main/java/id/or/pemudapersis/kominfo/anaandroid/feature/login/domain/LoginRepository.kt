package id.or.pemudapersis.kominfo.anaandroid.feature.login.domain

import id.or.pemudapersis.kominfo.anaandroid.base.network.AnaService
import id.or.pemudapersis.kominfo.anaandroid.feature.login.data.LoginResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface LoginRepository {
    fun postLogin(npa: String, password: String): Single<LoginResponse>
}

class LoginRepositoryImpl @Inject constructor(private val service: AnaService) : LoginRepository {
    override fun postLogin(npa: String, password: String): Single<LoginResponse> {
        return service.login(npa, password, grantType = "password")
    }
}