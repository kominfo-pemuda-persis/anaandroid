package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain

import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.base.network.AnaService
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaRequest
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.SendEmailRequest
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface CheckNpaRepository {
    fun postCheckNpa(npa: CheckNpaRequest): Single<BaseResponse<CheckNpaResponse>>
    fun sendEmail(data: SendEmailRequest): Single<BaseResponse<String>>
}

class CheckNpaRepositoryImpl @Inject constructor(private val service: AnaService) : CheckNpaRepository {
    override fun postCheckNpa(npa: CheckNpaRequest): Single<BaseResponse<CheckNpaResponse>> {
        return service.checkNpa(npa)
    }

    override fun sendEmail(data: SendEmailRequest): Single<BaseResponse<String>> {
        return service.sendEmail(data)
    }
}