package id.or.pemudapersis.kominfo.anaandroid.feature.main.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.R
import id.or.pemudapersis.kominfo.anaandroid.base.BaseActivity
import id.or.pemudapersis.kominfo.anaandroid.base.model.*
import id.or.pemudapersis.kominfo.anaandroid.databinding.ActivityMainBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.ui.DetailActivity
import id.or.pemudapersis.kominfo.anaandroid.feature.splash.ui.SplashActivity
import id.or.pemudapersis.kominfo.anaandroid.feature.wizard.ui.WizardActivity
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val viewModel by viewModels<MainViewModel>()

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    override fun setupView(binding: ActivityMainBinding) {
        initToolbar()
        setupClicks()
        setupObserver()
    }

    override fun setData() {
        viewModel.fetchDataAnggota()
        viewModel.fetchSummaryData()
        viewModel.fetchFotoDetail()
    }

    override fun setObserver(): FragmentActivity = this

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val mMenuInflater: MenuInflater = menuInflater
        mMenuInflater.inflate(R.menu.menu_dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_setting -> {
                val intent = Intent(this@MainActivity, WizardActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar() {
        with(binding.llToolbar.toolbar) {
            setSupportActionBar(this)
        }
    }

    private fun setupClicks() {
        with(binding) {
            // TODO: Hanya untuk cek logout
            lytCard.icLogo.setOnClickListener {
                doLogout()
            }
            imgClose.setOnClickListener {
                grpGreeting.visibility = View.GONE
            }
            lytCard.tvNameAnggota.setOnClickListener {
                val intent = Intent(this@MainActivity, DetailActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun setupObserver() {
        viewModel.state.observe(this) { state ->
            when (state) {
                is MainViewModel.MainViewState.ShowLoading -> {
                    showLoading()
                }
                is MainViewModel.MainViewState.HideLoading -> {
                    hideLoading()
                }
                is MainViewModel.MainViewState.EmptyResult -> {
                    Timber.d("empty result")
                }
                is MainViewModel.MainViewState.GetDataAnggota -> {
                    showAnggota(state.anggota)
                }
                is MainViewModel.MainViewState.ErrorState -> {
                    // TODO: show error
                    Timber.d("error: ${state.throwable}")
                    doLogout()
                }
                is MainViewModel.MainViewState.GetSummaries -> {
                    showMonografi(state.summariesData.monografiData)
                    showPerforma(state.summariesData.performaData)
                    showTotalAnggota(state.summariesData.statusKeanggotaan)
                }
                is MainViewModel.MainViewState.GetFotoDetail -> {
                    showFotoDetail(state.fotoDetail)
                }
                else -> {
                    Timber.d("else result")
                }
            }
        }
    }

    private fun showLoading() {
        with(binding) {
            progressbar.visibility = View.VISIBLE
            grpGreeting.visibility = View.GONE
            lytCard.root.visibility = View.GONE
        }
    }

    private fun hideLoading() {
        Timber.d("hide loading")
        binding.progressbar.visibility = View.GONE
    }

    private fun showAnggota(anggota: Anggota) {
        Timber.d("show anggota; $anggota")
        with(binding) {
            grpGreeting.visibility = View.VISIBLE
            lytCard.root.visibility = View.VISIBLE
            tvName.text = getString(R.string.text_name_greeting, anggota.nama.orEmpty())
            lytCard.tvNpaAnggota.text = anggota.npa.orEmpty()
            lytCard.tvNameAnggota.text = anggota.nama.orEmpty()
        }
    }

    private fun showMonografi(monografiItem: MonografiItem?) {
        monografiItem?.let { item ->
            with(binding.lytMonografi) {
                tvDataTitle.text = getString(R.string.text_data_monografi)
                root.visibility = View.VISIBLE
                lytPj.tvCount.text = item.monografiPj.toString()
                lytPj.tvLevel.text = getString(R.string.text_pj)
                lytPc.tvCount.text = item.monografiPc.toString()
                lytPc.tvLevel.text = getString(R.string.text_pc)
                lytPd.tvCount.text = item.monografiPd.toString()
                lytPd.tvLevel.text = getString(R.string.text_pd)
                lytPw.tvCount.text = item.monografiPw.toString()
                lytPw.tvLevel.text = getString(R.string.text_pw)
            }
        }
    }

    private fun showPerforma(performaItem: PerformaItem?) {
        performaItem?.let { item ->
            with(binding.lytPerforma) {
                tvDataTitle.text = getString(R.string.text_data_performa)
                root.visibility = View.VISIBLE
                lytPj.tvCount.text = item.performaPj.toString()
                lytPj.tvLevel.text = getString(R.string.text_pj)
                lytPc.tvCount.text = item.performaPc.toString()
                lytPc.tvLevel.text = getString(R.string.text_pc)
                lytPd.tvCount.text = item.performaPd.toString()
                lytPd.tvLevel.text = getString(R.string.text_pd)
                lytPw.tvCount.text = item.performaPw.toString()
                lytPw.tvLevel.text = getString(R.string.text_pw)
            }
        }
    }

    private fun showFotoDetail(fotoDetail: FotoDetail) {
        with(binding.lytCard) {
            Glide.with(this@MainActivity)
                .load(fotoDetail.imgScr)
                .placeholder(R.drawable.logo_ana)
                .error(R.drawable.logo_ana)
                .fitCenter()
                .into(imgPhoto)
        }
    }

    // TODO: The API still not ready for this section
    private fun showTotalAnggota(statusKeanggotaan: StatusKeanggotaan?) {
        statusKeanggotaan?.let {
            with(binding) {
                llAnggota.visibility = View.VISIBLE
                llUser.visibility = View.VISIBLE
                itemAnggota.tvCount.text = it.detailStatusList?.get(0)?.jumlah.toString()
                itemAnggota.tvLevel.text = getString(R.string.text_anggota_tersiar)
                itemUser.tvCount.text = it.detailStatusList?.get(1)?.jumlah.toString()
                itemUser.tvLevel.text = getString(R.string.text_anggota_biasa)
            }
        }
    }

    private fun doLogout() {
        viewModel.doLogout()
        val intent = Intent(this@MainActivity, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
