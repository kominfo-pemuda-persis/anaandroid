package id.or.pemudapersis.kominfo.anaandroid.feature.login.ui

import android.content.SharedPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.BaseViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.util.Constants
import id.or.pemudapersis.kominfo.anaandroid.feature.login.domain.LoginUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val useCase: LoginUseCase,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel<LoginViewModel.LoginState>() {

    fun postLogin(npa: String, password: String) {
        addDisposable.add(
            useCase.postLogin(npa, password)
                .doOnSubscribe { _state.postValue(LoginState.SHowLoading) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.accessToken?.isNotEmpty() == true) {
                        with(sharedPreferences.edit()) {
                            putString(Constants.ACCESS_TOKEN, it.accessToken.orEmpty())
                            apply()
                        }
                        _state.postValue(LoginState.LoginSuccess)
                    }
                }, {
                    //error
                    with(sharedPreferences.edit()) {
                        remove(Constants.ACCESS_TOKEN)
                        apply()
                    }
                    _state.postValue(LoginState.LoginFailed(it))
                })
        )
    }

    sealed class LoginState {
        object SHowLoading : LoginState()
        object HideLoading : LoginState()
        object LoginSuccess : LoginState()

        data class LoginFailed(val error: Throwable) : LoginState()
    }
}