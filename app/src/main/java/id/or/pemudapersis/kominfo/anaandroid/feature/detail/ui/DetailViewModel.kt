package id.or.pemudapersis.kominfo.anaandroid.feature.detail.ui

import dagger.hilt.android.lifecycle.HiltViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.BaseViewModel
import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailAnggotaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain.DetailAnggotaUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val useCase: DetailAnggotaUseCase
) : BaseViewModel<DetailViewModel.ResponseState>() {

    fun fetchDetailAnggota() {
        addDisposable.add(
            useCase.fetchDetailAnggota()
                .doOnSubscribe { _state.postValue(ResponseState.ShowLoading) }
                .doAfterTerminate { _state.postValue(ResponseState.HideLoading) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    // result response to display data
                    _state.value = ResponseState.ResponseData(result)
                }, {
                    // result response
                    _state.value = ResponseState.ResponseFailed(it, "result")
                })
        )
    }

    sealed class ResponseState {
        object ShowLoading : ResponseState()
        object HideLoading : ResponseState()
        object ResponseSuccess : ResponseState()

        data class ResponseFailed(val error: Throwable, val isResult: String) : ResponseState()
        data class ResponseData(val data: BaseResponse<DetailAnggotaResponse>) : ResponseState()
    }
}
