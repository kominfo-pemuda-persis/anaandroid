package id.or.pemudapersis.kominfo.anaandroid.di

import id.or.pemudapersis.kominfo.anaandroid.base.network.AnaService

interface AnaAppDeps {
    fun anaService(): AnaService
}