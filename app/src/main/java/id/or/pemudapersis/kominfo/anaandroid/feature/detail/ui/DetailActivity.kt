package id.or.pemudapersis.kominfo.anaandroid.feature.detail.ui

import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.R
import id.or.pemudapersis.kominfo.anaandroid.base.BaseActivity
import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseErrorResponse
import id.or.pemudapersis.kominfo.anaandroid.databinding.ActivityDetailBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.adapter.DetailKeluargaAdapter
import id.or.pemudapersis.kominfo.anaandroid.feature.adapter.DetailPendidikanAdapter
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailAnggotaResponse
import id.or.pemudapersis.kominfo.anaandroid.utils.SnackBarCustom
import retrofit2.HttpException
import java.lang.StringBuilder

@AndroidEntryPoint
class DetailActivity : BaseActivity<ActivityDetailBinding>() {
    private val viewModel by viewModels<DetailViewModel>()
    private lateinit var name: String
    override val bindingInflater: (LayoutInflater) -> ActivityDetailBinding
        get() = ActivityDetailBinding::inflate

    override fun setupView(binding: ActivityDetailBinding) {
        setupObserver()
    }

    override fun setData() {
        viewModel.fetchDetailAnggota()
    }

    override fun setObserver(): FragmentActivity = this

    private fun setupObserver() {
        viewModel.state.observe(this) { state ->
            when (state) {
                is DetailViewModel.ResponseState.ShowLoading -> {
                    showLoading(true)
                }
                is DetailViewModel.ResponseState.HideLoading -> {
                    showLoading(false)
                }
                is DetailViewModel.ResponseState.ResponseSuccess -> {
                    showLoading(false)
                }
                is DetailViewModel.ResponseState.ResponseFailed -> {
                    showLoading(false)
                    onCheckFailed(state.error, state.isResult)
                }
                is DetailViewModel.ResponseState.ResponseData -> {
                    showLoading(false)
                    state.data.data?.let {
                        setUpAnggotaLayout(it)
                        setUpKontakLayout(it)
                        setUpJamiyyahLayout(it)
                        setUpPendidikanLayout(it)
                        setUpKeluargaLayout(it)
                    }
                }
                else -> {}
            }
        }
    }

    private fun setUpAnggotaLayout(data: DetailAnggotaResponse) {
        with(binding) {
            name = data.nama.toString()
            lytAnggota.tvNamaData.text = data.nama
            lytAnggota.tvTempatLahirData.text = data.tempat
            lytAnggota.tvTanggalLahirData.text = data.tanggalLahir
            lytAnggota.tvGolonganDarahData.text = data.golDarah
            lytAnggota.tvStatusData.text = data.statusMerital
            lytAnggota.tvNpaData.text = data.npa
        }
        initToolbar(name)
    }

    private fun setUpKontakLayout(data: DetailAnggotaResponse) {
        with(binding) {
            lytKontak.tvEmailData.text = data.email
            lytKontak.tvTeleponData.text = data.noTelpon
        }
    }

    private fun setUpJamiyyahLayout(data: DetailAnggotaResponse) {
        with(binding) {
            lytJamiyyah.tvAlamatData.text = data.alamat
            lytJamiyyah.tvPimpinanJamaahData.text = data.namaPj
            lytJamiyyah.tvPimpinanCabangData.text = data.pimpinanCabang?.namaPimpinanCabang
            lytJamiyyah.tvPimpinanDaerahData.text = data.pimpinanDaerah?.namaPimpipnanDaerah
            lytJamiyyah.tvPimpinanWilayahData.text = data.pimpinanWilayah?.namaWilayah
            lytJamiyyah.tvPendaftaranAnggotaData.text = data.regDate
            lytJamiyyah.tvMasaAktifData.text = data.masaAktifKta
        }
    }

    private fun setUpKeluargaLayout(data: DetailAnggotaResponse) {
        val detailKeluargaAdapter = data.detailKeluarga?.let { DetailKeluargaAdapter(it) }
        val layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        FlexboxLayoutManager(this)
        val recyclerView = binding.recyclerViewDataKeluarga
        recyclerView.apply {
            this.adapter = detailKeluargaAdapter
            this.setHasFixedSize(true)
            this.layoutManager = layoutManager
        }
    }

    private fun setUpPendidikanLayout(data: DetailAnggotaResponse) {
        val detailPendidikanAdapter = data.detailPendidikan?.let { DetailPendidikanAdapter(it) }
        val layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        FlexboxLayoutManager(this)
        val recyclerView = binding.recyclerViewDataPendidikan
        recyclerView.apply {
            this.adapter = detailPendidikanAdapter
            this.setHasFixedSize(true)
            this.layoutManager = layoutManager
        }
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            if (isLoading) {
                progressBarDetail.visibility = View.VISIBLE
            } else {
                progressBarDetail.visibility = View.GONE
            }
        }
    }

    private fun onCheckFailed(throwable: Throwable, isResult: String) {
        val message = when (throwable) {
            is HttpException -> {
                Gson().fromJson(
                    throwable.response()?.errorBody()?.string(),
                    BaseErrorResponse::class.java
                ).errors?.get(0)
            }
            else -> throwable.message
        }

        SnackBarCustom.snackBarIconInfo(
            binding.root,
            layoutInflater,
            binding.root.context,
            message.toString(),
            R.drawable.ic_attention_outline,
            R.color.red_500
        )
    }

    private fun initToolbar(name: String) {
        with(binding.toolbarDetail.toolbar) {
            setSupportActionBar(this)
        }
        binding.toolbarDetail.tvToolbarTitle.text = StringBuilder("Detail ").append("'${name}'")
    }
}
