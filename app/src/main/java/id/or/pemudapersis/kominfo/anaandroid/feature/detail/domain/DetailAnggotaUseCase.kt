package id.or.pemudapersis.kominfo.anaandroid.feature.detail.domain

import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailAnggotaResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface DetailAnggotaUseCase {
    fun fetchDetailAnggota(): Single<BaseResponse<DetailAnggotaResponse>>
}

class DetailAnggotaUseCaseImpl @Inject constructor(private val repository: DetailAnggotaRepository) :
    DetailAnggotaUseCase {
    override fun fetchDetailAnggota(): Single<BaseResponse<DetailAnggotaResponse>> {
        return repository.fetchDetailAnggota()
    }
}
