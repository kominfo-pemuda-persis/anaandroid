package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.ui

import android.content.Intent
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.R
import id.or.pemudapersis.kominfo.anaandroid.base.BaseActivity
import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseErrorResponse
import id.or.pemudapersis.kominfo.anaandroid.databinding.ActivityCheckNpaBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.data.CheckNpaResponse
import id.or.pemudapersis.kominfo.anaandroid.feature.login.ui.LoginActivity
import id.or.pemudapersis.kominfo.anaandroid.utils.SnackBarCustom
import id.or.pemudapersis.kominfo.anaandroid.utils.hideKeyboard
import retrofit2.HttpException
import timber.log.Timber

@AndroidEntryPoint
class CheckNpaActivity: BaseActivity<ActivityCheckNpaBinding>() {
    private val viewModel by viewModels<CheckNpaViewModel>()

    override val bindingInflater: (LayoutInflater) -> ActivityCheckNpaBinding
        get() = ActivityCheckNpaBinding::inflate

    override fun setupView(binding: ActivityCheckNpaBinding) {
        setupClick()
        setupObserver()
        setupFormListener()
    }

    override fun setData() {
        //bo implementation required

    }

    override fun setObserver(): FragmentActivity = this

    private fun setupClick() {
        with(binding) {
            btCheckNpa.setOnClickListener {
                val npa = etNpa.text.toString().trim()
                if (npa.isEmpty()) {
                    tilCheckNpa.error = getString(R.string.text_npa_empty)
                }
                if (npa.isNotEmpty()) {
                    viewModel.postCheckNpa(npa)
                }
            }

            etNpa.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                val npa = etNpa.text.toString().trim()
                if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    hideKeyboard()
                    viewModel.postCheckNpa(npa)
                    return@OnKeyListener true
                }
                false
            })

            btSendEmail.setOnClickListener {
                with(binding) {
                    viewModel.postSendEmail(
                        tvValueNpa.text.toString(),
                        tvValueEmail.text.toString()
                    )
                }
            }

            lvLoginHere.setOnClickListener {
                finish()
            }
        }
    }

    private fun setupFormListener() {
        with(binding) {
            tilCheckNpa.editText?.doOnTextChanged { text, _, _, _ ->
                if (text?.isEmpty() == true) {
                    tilCheckNpa.error = getString(R.string.text_npa_empty)
                } else {
                    tilCheckNpa.error = null
                    tilCheckNpa.isErrorEnabled = false
                }
            }
        }
    }

    private fun setupObserver() {
        viewModel.state.observe(this) { state ->
            when (state) {
                is CheckNpaViewModel.ResponseState.ShowLoading -> {
                    showLoading(true)
                }
                is CheckNpaViewModel.ResponseState.HideLoading -> {
                    showLoading(false)
                }
                is CheckNpaViewModel.ResponseState.ResponseSuccess -> {
                    showLoading(false)
                }
                is CheckNpaViewModel.ResponseState.ResponseFailed -> {
                    showLoading(false)
                    onCheckFailed(state.error, state.isResult)
                }
                is CheckNpaViewModel.ResponseState.ResponseData -> {
                    showLoading(false)
                    state.data.data?.let {
                        setUpDetailScreen(it)
                    }
                }
                is CheckNpaViewModel.ResponseState.ResponseSendEmail -> {
                    showLoading(false)
                    state.data.data?.let {
                        Timber.e(it)
                    }
                }
            }
        }
    }

    private fun setUpDetailScreen(data: CheckNpaResponse) {
        with(binding) {
            tvValueNpa.text = data.npa
            tvValueNpaName.text = data.nama
            tvValueEmail.text = data.email

            displayView(View.VISIBLE)
        }
    }

    private fun onCheckFailed(throwable: Throwable, isResult: String) {
        val message = when (throwable) {
            is HttpException -> {
                if (isResult == "result") displayView(View.GONE) else displayView(View.VISIBLE)
                Gson().fromJson(
                    throwable.response()?.errorBody()?.string(),
                    BaseErrorResponse::class.java
                ).errors?.get(0)
            }
            else -> throwable.message
        }

        SnackBarCustom.snackBarIconInfo(
            binding.root, layoutInflater, binding.root.context,
            message.toString(),
            R.drawable.ic_attention_outline, R.color.red_500
        )
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            if (isLoading) {
                pb.visibility = View.VISIBLE
                enableView(!isLoading)
            } else {
                pb.visibility = View.GONE
                enableView(!isLoading)
            }
        }
    }

    private fun enableView(isEnabled: Boolean) {
        with(binding) {
            btCheckNpa.isEnabled = isEnabled
            etNpa.isEnabled = isEnabled
        }
    }

    private fun displayView(isDisplay: Int) {
        with(binding) {
            relativeLayout.visibility = isDisplay
            linearLayout.visibility = isDisplay
        }
    }
}