package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain.CheckNpaUseCase
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain.CheckNpaUseCaseImpl

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @Binds
    @ViewModelScoped
    abstract fun bindsCheckNpaUseCase(
        checkNpaUseCaseImpl: CheckNpaUseCaseImpl
    ): CheckNpaUseCase
}