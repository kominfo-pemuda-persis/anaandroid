package id.or.pemudapersis.kominfo.anaandroid.feature.wizard.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.databinding.FragmentStep4DataKontakBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.wizard.callback.WizardCallback

@AndroidEntryPoint
class Step4DataKontakFragment: Fragment() {
    private var _binding: FragmentStep4DataKontakBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStep4DataKontakBinding.inflate(inflater, container, false)

        passData()

        return binding.root
    }

    private fun passData() {
        (activity as WizardActivity).setWizardCallback(object : WizardCallback {
            override fun onDataPass(): Boolean {
                return true
            }
        })
    }
}