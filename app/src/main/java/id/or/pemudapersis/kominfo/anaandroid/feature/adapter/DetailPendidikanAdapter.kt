package id.or.pemudapersis.kominfo.anaandroid.feature.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.or.pemudapersis.kominfo.anaandroid.databinding.ItemDataPendidikanBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.detail.data.response.DetailPendidikan

class DetailPendidikanAdapter(private val detailsPendidikan: List<DetailPendidikan>) :
    RecyclerView.Adapter<DetailPendidikanAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemDataPendidikanBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val detailPendidikan = detailsPendidikan[position]
        holder.bind(detailPendidikan)
    }

    override fun getItemCount() = detailsPendidikan.size

    inner class ViewHolder(private var binding: ItemDataPendidikanBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(detailPendidikan: DetailPendidikan) {
            binding.apply {
                tvInstansiData.text = detailPendidikan.instansi
                tvJurusanData.text = detailPendidikan.jurusan
                tvTahunData.text = StringBuilder(detailPendidikan.tahunMasuk.toString())
                    .append(" - ")
                    .append(detailPendidikan.tahunKeluar.toString())
                tvJenisData.text = detailPendidikan.jenisPendidikan
                tvJenisData.text = detailPendidikan.jenisPendidikan
                tvKampusData.text = detailPendidikan.instansi
                tvJurusanKampusData.text = detailPendidikan.jurusan
            }
        }
    }
}
