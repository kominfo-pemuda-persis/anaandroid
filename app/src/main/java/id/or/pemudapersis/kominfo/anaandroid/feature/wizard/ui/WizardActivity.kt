package id.or.pemudapersis.kominfo.anaandroid.feature.wizard.ui

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import dagger.hilt.android.AndroidEntryPoint
import id.or.pemudapersis.kominfo.anaandroid.R
import id.or.pemudapersis.kominfo.anaandroid.base.BaseActivity
import id.or.pemudapersis.kominfo.anaandroid.databinding.ActivityWizardBinding
import id.or.pemudapersis.kominfo.anaandroid.feature.wizard.callback.WizardCallback
import id.or.pemudapersis.kominfo.anaandroid.utils.setToolbar
import timber.log.Timber

@AndroidEntryPoint
class WizardActivity: BaseActivity<ActivityWizardBinding>() {
    private var maxStep = 8
    private var currentStep = 1

    override val bindingInflater: (LayoutInflater) -> ActivityWizardBinding
        get() = ActivityWizardBinding::inflate

    override fun setupView(binding: ActivityWizardBinding) {
        initToolbar()
        setupClicks()
        initComponent()
        setupObserver()
    }

    override fun setData() {

    }

    override fun setObserver(): FragmentActivity = this

    private fun initToolbar() {
        with(binding.llToolbar) {
            this.title.text = resources.getString(R.string.text_setting)
            val toolbar = this.toolbar
            toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white)
            setToolbar(this@WizardActivity, toolbar)
        }
    }

    private fun setupClicks() {
        with(binding) {
            btnBack.setOnClickListener {
                backStep(currentStep)
                bottomProgressDots(currentStep)
            }

            btnNext.setOnClickListener {
                nextStep(currentStep)
                bottomProgressDots(currentStep)
            }
        }
    }

    private fun initComponent() {
        if (currentStep == 1) {
            binding.btnBack.visibility = View.INVISIBLE
        }

        bottomProgressDots(currentStep)

        val mFragmentManager = supportFragmentManager
        val mHomeFragment = Step1DataAnggotaFragment()
        val fragment = mFragmentManager.findFragmentByTag(Step1DataAnggotaFragment::class.java.simpleName)

        if(fragment !is Step1DataAnggotaFragment){
            binding.tvLayoutTitle.text = resources.getText(R.string.data_anggota)
            val args = Bundle()
            //args.putString(KEY_CONST, data)
            mHomeFragment.arguments = args
            mFragmentManager
                .beginTransaction()
                .add(R.id.frame_container, mHomeFragment, Step1DataAnggotaFragment::class.java.simpleName)
                .commit()
        }
    }

    private fun currentStepCondition(currentStep: Int) {
        val mFragmentManager = supportFragmentManager
        when (currentStep) {
            1 -> {
                Timber.e("### step 1 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_anggota)
                val fragment = Step1DataAnggotaFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step1DataAnggotaFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            2 -> {
                Timber.e("### step 2 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_keanggotaan)
                val fragment = Step2DataKeanggotaanFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step2DataKeanggotaanFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            3 -> {
                Timber.e("### step 3 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_alamat)
                val fragment = Step3DataAlamatFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step3DataAlamatFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            4 ->{
                Timber.e("### step 4 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_kontak)
                val fragment = Step4DataKontakFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step4DataKontakFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }

            5 -> {
                Timber.e("### step 5 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_keluarga)
                val fragment = Step5DataKeluargaFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step5DataKeluargaFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            6 -> {
                Timber.e("### step 6 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_pendidikan)
                val fragment = Step6DataPendidikanFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step6DataPendidikanFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            7 -> {
                Timber.e("### step 7 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_organisasi)
                val fragment = Step7DataOrganisasiFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step7DataOrganisasiFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
            8 -> {
                Timber.e("### step 8 = $currentStep")
                binding.tvLayoutTitle.text = resources.getString(R.string.data_tafiq)
                val fragment = Step8DataTafiqFragment()
                val args = Bundle()
                //args.putString(KEY_CONST, data)
                fragment.arguments = args
                mFragmentManager.beginTransaction().apply {
                    replace(R.id.frame_container, fragment, Step8DataTafiqFragment::class.java.simpleName)
                    addToBackStack(null)
                    commit()
                }
            }
        }
    }

    private lateinit var wizardCallback: WizardCallback
    fun setWizardCallback(wizardCallback: WizardCallback) {
        this.wizardCallback = wizardCallback
    }

    private fun nextStep(progress: Int) {
        val passData =  wizardCallback.onDataPass()
        if (passData) {
            if (progress < maxStep) {
                currentStep = progress + 1
                currentStepCondition(currentStep)

                binding.apply {
                    btnBack.visibility = View.VISIBLE
                    btnNext.visibility = View.VISIBLE
                    btnSave.visibility = View.GONE

                    if (currentStep == maxStep) {
                        btnNext.visibility = View.GONE
                        btnSave.visibility = View.VISIBLE
                    }
                }
            } else {
                finish()
            }
        }
    }

    private fun backStep(progress: Int) {
        if (progress > 1) {
            currentStep = progress-1
            currentStepCondition(currentStep)

            binding.apply {
                btnNext.visibility = View.VISIBLE
                btnSave.visibility = View.GONE

                if (currentStep == 1) {
                    btnBack.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun bottomProgressDots(current_index: Int) {
        val currentIndex = current_index - 1
        val dotsLayout = binding.layoutDots
        val dots = arrayOfNulls<ImageView>(maxStep)

        dotsLayout.removeAllViews()
        for (i in dots.indices) {
            dots[i] = ImageView(this)
            val widthHeight = 15
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams(widthHeight, widthHeight))
            params.setMargins(10, 10, 10, 10)
            dots[i]!!.layoutParams = params
            dots[i]!!.setImageResource(R.drawable.shape_circle)
            dots[i]!!.setColorFilter(ContextCompat.getColor(this, R.color.grey_20), PorterDuff.Mode.SRC_IN)
            dotsLayout.addView(dots[i])
        }
        if (dots.isNotEmpty()) {
            dots[currentIndex]!!.setImageResource(R.drawable.shape_circle)
            dots[currentIndex]!!.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN)
        }
    }

    private fun setupObserver() {

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}