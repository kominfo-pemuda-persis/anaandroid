package id.or.pemudapersis.kominfo.anaandroid.feature.main.data

import id.or.pemudapersis.kominfo.anaandroid.base.model.*
import id.or.pemudapersis.kominfo.anaandroid.base.network.AnaService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface MainRepository {
  fun fetchAnggota(): Single<BaseResponse<Anggota>>
  fun fetchSummariesMonografi(): Single<BaseResponse<MonografiItem>>
  fun fetchSummariesPerforma(): Single<BaseResponse<PerformaItem>>
  fun fetchSummariesStatusKeanggotaan(): Single<BaseResponse<StatusKeanggotaan>>
  fun fetchFotoDetail(): Single<BaseResponse<FotoDetail>>
}

class MainRepositoryImpl @Inject constructor(private val anaService: AnaService) : MainRepository {
  override fun fetchAnggota(): Single<BaseResponse<Anggota>> {
    return anaService.fetchAnggota()
  }

  override fun fetchSummariesMonografi(): Single<BaseResponse<MonografiItem>> {
    return anaService.fetchSummariesMonografi()
  }

  override fun fetchSummariesPerforma(): Single<BaseResponse<PerformaItem>> {
    return anaService.fetchSummariesPerforma()
  }

  override fun fetchSummariesStatusKeanggotaan(): Single<BaseResponse<StatusKeanggotaan>> {
    return anaService.fetchStatusKeanggotaan()
  }

  override fun fetchFotoDetail(): Single<BaseResponse<FotoDetail>> {
    return anaService.fetchFotoDetail()
  }
}