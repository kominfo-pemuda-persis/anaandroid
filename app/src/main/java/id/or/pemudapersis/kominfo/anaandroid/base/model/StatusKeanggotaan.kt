package id.or.pemudapersis.kominfo.anaandroid.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 *  Created by hanhan.firmansyah on 26/08/22
 */
@Parcelize
data class StatusKeanggotaan(
  @field:SerializedName("detailStatusList") val detailStatusList: List<DetailStatus>? = emptyList(),
  @field:SerializedName("jumlahTotal") val jumlahTotal: Int? = null
): Parcelable
