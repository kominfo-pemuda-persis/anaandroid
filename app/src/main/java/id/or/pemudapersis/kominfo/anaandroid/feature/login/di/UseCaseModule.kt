package id.or.pemudapersis.kominfo.anaandroid.feature.login.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.or.pemudapersis.kominfo.anaandroid.feature.login.domain.LoginUseCase
import id.or.pemudapersis.kominfo.anaandroid.feature.login.domain.LoginUseCaseImpl

/**
 * Created by Julsapargi Nursam on 20/08/22
 * Mobile Engineer - Android
 */

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @Binds
    @ViewModelScoped
    abstract fun bindsLoginUseCase(
        loginUseCaseImpl: LoginUseCaseImpl
    ): LoginUseCase
}