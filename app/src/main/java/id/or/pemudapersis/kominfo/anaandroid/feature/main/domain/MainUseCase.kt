package id.or.pemudapersis.kominfo.anaandroid.feature.main.domain

import id.or.pemudapersis.kominfo.anaandroid.base.model.BaseResponse
import id.or.pemudapersis.kominfo.anaandroid.base.model.Anggota
import id.or.pemudapersis.kominfo.anaandroid.base.model.FotoDetail
import id.or.pemudapersis.kominfo.anaandroid.feature.main.data.MainRepository
import id.or.pemudapersis.kominfo.anaandroid.feature.main.model.SummariesData
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface MainUseCase {
  fun fetchAnggota(): Single<BaseResponse<Anggota>>
  fun fetchSummaries(): Single<SummariesData>
  fun fetchFotoDetail(): Single<BaseResponse<FotoDetail>>
}

class MainUseCaseImpl @Inject constructor(private val repository: MainRepository): MainUseCase {
  override fun fetchAnggota(): Single<BaseResponse<Anggota>> {
    return repository.fetchAnggota()
  }

  override fun fetchSummaries(): Single<SummariesData> {
    return Single.zip(
      repository.fetchSummariesMonografi(),
      repository.fetchSummariesPerforma(),
      repository.fetchSummariesStatusKeanggotaan()
    ) { monografiResponse, performaResponse, statusKeanggotaanResponse ->
      if (monografiResponse.status == "OK" && performaResponse.status == "OK" && statusKeanggotaanResponse.status == "OK") {
        SummariesData(monografiResponse.data, performaResponse.data, statusKeanggotaanResponse.data)
      } else {
        error("error")
      }
    }
  }

  override fun fetchFotoDetail(): Single<BaseResponse<FotoDetail>> {
    return repository.fetchFotoDetail()
  }
}