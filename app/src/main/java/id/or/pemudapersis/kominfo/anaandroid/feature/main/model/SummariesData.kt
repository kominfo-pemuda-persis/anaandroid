package id.or.pemudapersis.kominfo.anaandroid.feature.main.model

import android.os.Parcelable
import id.or.pemudapersis.kominfo.anaandroid.base.model.MonografiItem
import id.or.pemudapersis.kominfo.anaandroid.base.model.PerformaItem
import id.or.pemudapersis.kominfo.anaandroid.base.model.StatusKeanggotaan
import kotlinx.parcelize.Parcelize

/**
 *  Created by hanhan.firmansyah on 26/08/22
 */
@Parcelize
data class SummariesData(
  val monografiData: MonografiItem? = null,
  val performaData: PerformaItem? = null,
  val statusKeanggotaan: StatusKeanggotaan? = null
) : Parcelable
