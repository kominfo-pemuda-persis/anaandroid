package id.or.pemudapersis.kominfo.anaandroid.feature.main.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import id.or.pemudapersis.kominfo.anaandroid.feature.main.domain.MainUseCase
import id.or.pemudapersis.kominfo.anaandroid.feature.main.domain.MainUseCaseImpl

/**
 * Created by Julsapargi Nursam on 20/08/22
 * Mobile Engineer - Android
 */

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @Binds
    @ViewModelScoped
    abstract fun bindsMainUseCase(
        mainUseCaseImpl: MainUseCaseImpl
    ): MainUseCase
}