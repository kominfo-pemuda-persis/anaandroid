package id.or.pemudapersis.kominfo.anaandroid.feature.login.domain

import id.or.pemudapersis.kominfo.anaandroid.feature.login.data.LoginResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

interface LoginUseCase {
    fun postLogin(npa: String, password: String): Single<LoginResponse>
}

class LoginUseCaseImpl @Inject constructor(private val repository: LoginRepository): LoginUseCase {
    override fun postLogin(npa: String, password: String): Single<LoginResponse> {
        return repository.postLogin(npa, password)
    }
}