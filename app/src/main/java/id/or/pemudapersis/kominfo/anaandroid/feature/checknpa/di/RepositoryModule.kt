package id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain.CheckNpaRepository
import id.or.pemudapersis.kominfo.anaandroid.feature.checknpa.domain.CheckNpaRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindsCheckNpaRepository(
        checkNpaIRepositoryImpl: CheckNpaRepositoryImpl
    ): CheckNpaRepository
}